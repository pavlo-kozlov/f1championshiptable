F1 Championship results table.

Application represents the list of F1 winners by season. Every row is a season (year) and the name of winner.

User can see the list of race winners during particular year by simply clicking on the row. This data is loaded on demand just before being showed. The driver who is a world champion this year is highlighted in the list of race winners. 

# Installation
After cloning the project, follow these steps to get started.

With Vagrant, the server will be available on [port 8080](http://localhost:8080/) and without Vagrant it will be available on [port 8181](http://localhost:8181/).

## Linux and OS X
### With Vagrant
```
npm run vagrant; //Starts the virtual machine
```

### Without Vagrant
```
npm install; //Performs a clean installation of all node dependencies
```

## Windows
### Without Vagrant
```
npm install; //Performs a clean installation of all node dependencies
```

# Development

## Linux and OS X
### With Vagrant
No extra actions required since server is started during `vagrant up` execution

### Without Vagrant
```
npm run webpack-server; //Starts the webserver
```

## Windows
### Without Vagrant
```
npm run webpack-server; //Starts the webserver
```

# Building code for production
```
npm run compile; //Installs all dependencies and compiles source code into bin/js and bin/css folders
```
You can see the result by opening `./output/index.html` file in web browser