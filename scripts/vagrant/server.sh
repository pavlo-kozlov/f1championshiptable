#!/usr/bin/env bash

set -e;

echo "Starting development server...";
cd /vagrant;
    > /vagrant/server.log;
    npm run webpack-server > /vagrant/server.log 2>&1 &
echo "Started development server at http://localhost:8080/ !";
