#!/usr/bin/env bash

set -e;

echo "Installing APT packages...";
curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -;
sudo apt-get install -y nodejs build-essential;
echo "Finished Installing APT packages.";

echo "Installing Yarn...";
npm install yarn -g;
echo "Finished installing Yarn.";

echo "Clearing npm packages...";
cd /vagrant;
    rm -rf yarn.lock;
    rm -rf node_modules;
echo "Finished clearing npm packages...";

echo "Installing npm packages...";
cd /vagrant;
    yarn install --no-lockfile;
echo "Finished installing npm packages.";
