const path = require('path');
const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports =  {
        target: 'web',
        devtool: 'source-map',

        entry: './src/index.js',

        output: {
            path: path.resolve(__dirname, '../bin'),
            filename: `js/bundle.min.js`
        },

        resolve: {
            extensions: ['.js', '.jsx']
        },

        module: {
            rules: [
                {
                    test: /\.(js|jsx)$/,
                    loader: 'babel-loader'
                },

                {
                    test: /\.(sass|scss)$/,
                    use: ExtractTextPlugin.extract({
                        fallbackLoader: 'style-loader',
                        loader: 'css-loader!sass-loader'
                    })
                }
            ]
        },

        plugins: [
            new webpack.DefinePlugin({
                'process.env': {
                    NODE_ENV: JSON.stringify('production')
                }
            }),

            new ExtractTextPlugin({
                filename: 'css/bundle.min.css',
                allChunks: true
            }),

            new webpack.optimize.UglifyJsPlugin({
                sourceMap: true,

                compress: {
                    warnings: false,
                    screw_ie8: true,
                    conditionals: true,
                    unused: true,
                    comparisons: true,
                    sequences: true,
                    dead_code: true,
                    evaluate: true,
                    if_return: true,
                    join_vars: true
                },

                output: {
                    comments: false
                }
            })
        ]
};
