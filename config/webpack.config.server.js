const path = require('path');
const webpack = require('webpack');

const isVagrant = (/vagrant/i).test(process.env.PWD);

module.exports = {
    target: 'web',
    devtool: 'cheap-module-eval-source-map',

    entry: './src/index.js',

    output: {
        path: path.resolve(__dirname, '../bin'),
        filename: 'js/dev/bundle.min.js'
    },

    resolve: {
        extensions: ['.js', '.jsx']
    },

    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                loader: 'babel-loader'
            },

            {
                test: /\.(sass|scss)$/,
                use: [
                    'style-loader',
                    'css-loader',
                    'sass-loader'
                ]
            }
        ]
    },

    plugins: [
        new webpack.DefinePlugin({
            'process.env': {
                NODE_ENV: JSON.stringify('development')
            }
        }),

        new webpack.HotModuleReplacementPlugin()
    ],

    devServer: {
        hot: true,
        https: false,
        hotOnly: false,
        compress: false,
        watchContentBase: true,
        host: '0.0.0.0',
        contentBase: path.resolve(__dirname, '../public'),
        port: isVagrant ? 8080 : 8181,
        watchOptions: {
            aggregateTimeout: 0,
            poll: isVagrant ? 1000 : false
        }
    }
};
