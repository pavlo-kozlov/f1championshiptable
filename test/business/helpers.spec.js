import {parseSeasonWinner, parseSeasonRaces, compareSeasons} from '../../src/business/helpers';

const SEASON_WINNER_JSON = require('../../__mocks__/SeasonWinner.json');

const SEASON_RACES_JSON = require('../../__mocks__/SeasonRaces.json');

describe('helper parseSeasonWinner', () => {

    it('should parse object and return season winner data', () => {
        const winner = parseSeasonWinner(SEASON_WINNER_JSON);

        expect(winner.name).toEqual('Fernando Alonso');
        expect(winner.season).toEqual(2005);
        expect(winner.driverId).toEqual('alonso');
    });
});

describe('helper parseSeasonRaces', () => {

    it('should parse object and return season races data', () => {
        const races = parseSeasonRaces(SEASON_RACES_JSON);

        expect(races instanceof Array).toBeTruthy();
        expect(races.length).toEqual(18);
        expect(races[0]).toEqual(expect.objectContaining({
            raceName: 'Australian Grand Prix',
            driverId: 'hamilton',
            name: 'Lewis Hamilton'
        }));
        expect(races[11]).toEqual(expect.objectContaining({
            raceName: 'European Grand Prix',
            driverId: 'massa',
            name: 'Felipe Massa'
        }));
    });
});

describe('helper compareSeasons', () => {

    it('should return -1 if season of `a` smaller then season of `b`', () => {
        expect(compareSeasons({season: 2005}, {season: 2010})).toEqual(-1);
        expect(compareSeasons({season: 2007}, {season: 2015})).toEqual(-1);
    })

    it('should return 1 if season of `a` bigger or equal then season of `b`', () => {
        expect(compareSeasons({season: 2010}, {season: 2010})).toEqual(1);
        expect(compareSeasons({season: 2007}, {season: 2005})).toEqual(1);
        expect(compareSeasons({season: 20017}, {season: 2007})).toEqual(1);
    })
});
