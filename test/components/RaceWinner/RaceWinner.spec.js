import React from 'react';
import {shallow} from 'enzyme';

import RaceWinner from '../../../src/components/RaceWinner';

describe('<RaceWinner/>', () => {

    const WINNER = {
        raceName: 'Australian Grand Prix',
        name: 'Lewis Hamilton'
    };

    it('should render <RaceWinner/> component', () => {
        const wrapper = shallow(
            <RaceWinner winner={WINNER}/>
        );
        expect(wrapper.text()).toEqual(`- ${WINNER.name} (${WINNER.raceName})`);
        expect(wrapper.hasClass('race-winner')).toEqual(true);
        expect(wrapper.hasClass('highlighted')).toEqual(false);
    });

    it('should render <RaceWinner/> as highlighted when `isHighlighted` flag set to true', () => {
        const wrapper = shallow(
            <RaceWinner winner={WINNER} isHighlighted={true}/>
        );
        expect(wrapper.text()).toEqual(`- ${WINNER.name} (${WINNER.raceName})`);
        expect(wrapper.hasClass('race-winner')).toEqual(true);
        expect(wrapper.hasClass('highlighted')).toEqual(true);
    });
});
