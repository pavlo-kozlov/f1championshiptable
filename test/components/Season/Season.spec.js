import React from 'react';
import {shallow} from 'enzyme';

import Season from '../../../src/components/Season';
import RaceWinner from '../../../src/components/RaceWinner';

describe('<Season/>', () => {
    
    const WINNER = {
        season: 2005,
        name: 'Fernando Alonso',
        driverId: 'alonso'
    };

    const SEASON_ITEM = {
        season: 2005,
        winner:  WINNER,
        raceWinners: [],
        isSelected: false
    };
    
    it('should render <Season/> component', () => {
        const onSelectItem = jest.fn();
        const wrapper = shallow(
            <Season selectItem={onSelectItem} item={SEASON_ITEM}/>
        );

        expect(wrapper.find('.season-year').text()).toEqual('2005');
        expect(wrapper.find('.season-winner').text()).toEqual('2005Fernando Alonso');
        expect(wrapper.find('.season-winner').hasClass('.active')).toEqual(false);
        expect(wrapper.find('.races-list').length).toEqual(0);
    });

    it('should render <Season/> component with `active` class when `isSelected` === true', () => {
        const onSelectItem = jest.fn();
        const wrapper = shallow(
            <Season selectItem={onSelectItem} item={SEASON_ITEM} isSelected={true}/>
        );

        expect(wrapper.find('.season-winner').hasClass('active')).toEqual(true);
    });

    it('should render <Season/> component and call `selectItem` function when clicked on element', () => {
        const onSelectItem = jest.fn();
        const wrapper = shallow(
            <Season selectItem={onSelectItem} item={SEASON_ITEM}/>
        );

        wrapper.find('.season-winner').simulate('click');
        expect(onSelectItem).toHaveBeenCalledTimes(1);
    });

    it('should render <Season/> component with race winners if there are items in `raceWinner` array', () => {
        const seasonItem = {
            ...SEASON_ITEM,
            raceWinners: [
                {
                    raceName: 'Australian Grand Prix',
                    driverId: 'hamilton',
                    name: 'Lewis Hamilton'
                }, {
                    raceName: 'European Grand Prix',
                    driverId: 'massa',
                    name: 'Felipe Massa'
                },
                {
                    raceName: 'European Grand Prix',
                    driverId: 'alonso',
                    name: 'Fernando Alonso'
                }
            ]
        };
        const onSelectItem = jest.fn();
        const wrapper = shallow(
            <Season selectItem={onSelectItem} item={seasonItem} isSelected={true}/>
        );

        expect(wrapper.find('.races-list').length).toEqual(1);
        expect(wrapper.find('.races-list').contains(
            <RaceWinner isHighlighted={false} winner={seasonItem.raceWinners[0]}/>
        )).toEqual(true);
        expect(wrapper.find('.races-list').contains(
            <RaceWinner isHighlighted={true} winner={seasonItem.raceWinners[2]}/>
        )).toEqual(true);

    });

    it('should render <Season/> component without race winners if there are items in `raceWinner` array and `isSelected` === false', () => {
        const seasonItem = {
            ...SEASON_ITEM,
            raceWinners: [
                {
                    raceName: 'Australian Grand Prix',
                    driverId: 'hamilton',
                    name: 'Lewis Hamilton'
                }, {
                    raceName: 'European Grand Prix',
                    driverId: 'massa',
                    name: 'Felipe Massa'
                },
                {
                    raceName: 'European Grand Prix',
                    driverId: 'alonso',
                    name: 'Fernando Alonso'
                }
            ]
        };
        const onSelectItem = jest.fn();
        const wrapper = shallow(
            <Season selectItem={onSelectItem} item={seasonItem} isSelected={false}/>
        );
        expect(wrapper.find('.races-list').length).toEqual(0);
    });
});
