import React from 'react';
import PropTypes from 'prop-types';
import RaceWinner, {raceWinnerPropType} from '../RaceWinner';

import './Season.scss';

/**
 * Display season winner and winner of every race in the same year if selected.
 */
const Season = ({item, selectItem, isSelected}) => {
    const {season, raceWinners, winner: seasonWinner} = item;

    const seasonRaces = (isSelected && raceWinners.length > 0)
        ? <div className="races-list">
            {raceWinners.map((raceWinner) =>
                <RaceWinner key={raceWinner.raceName} isHighlighted={seasonWinner.driverId === raceWinner.driverId}
                            winner={raceWinner}/>
            )}
        </div>
        : '';

    return (
        <div>
            <div onClick={() => selectItem(season)} className={`season-winner${isSelected ? ' active' : ''}`}>
                <span className="season-year">{season}</span>
                {seasonWinner.name}
            </div>
            {seasonRaces}
        </div>
    );
}

export const seasonPropType = PropTypes.shape({
    winner: PropTypes.shape({
        season: PropTypes.number,
        name: PropTypes.string,
        driverId: PropTypes.string
    }).isRequired,
    raceWinners: PropTypes.arrayOf(
        raceWinnerPropType
    ).isRequired
});

Season.propTypes = {
    item: seasonPropType.isRequired,
    selectItem: PropTypes.func.isRequired,
    isSelected: PropTypes.bool
};

export default Season;
