import React from 'react';
import PropTypes from 'prop-types';

import './RaceWinner.scss';

/**
 * Display the season winner.
 */
const RaceWinner = ({isHighlighted, winner}) => (
    <div className={`race-winner${isHighlighted ? ' highlighted' : ''}`}>
        - {`${winner.name} (${winner.raceName})`}
    </div>
);

export const raceWinnerPropType = PropTypes.shape({
    driverId: PropTypes.string,
    name: PropTypes.string,
    raceName: PropTypes.string
});

RaceWinner.propTypes = {
    isHighlighted: PropTypes.bool,
    winner: raceWinnerPropType.isRequired
};

export default RaceWinner;
