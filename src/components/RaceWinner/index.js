import RaceWinner, {raceWinnerPropType} from './RaceWinner';
export {
    RaceWinner as default,
    raceWinnerPropType
};
