import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';

import {fetchSeasonRaces} from '../../actions';
import {seasonPropType} from '../Season';
import SeasonsList from './SeasonsList';

class SeasonsListContainer extends Component {

    /**
     * Constructor.
     */
    constructor() {
        super();

        this.state = {
            selectedSeason: -1
        };
    }

    /**
     * Select season
     *
     * @param {Number} season
     */
    selectSeason = (season) => {
        this.setState({
            selectedSeason: season !== this.state.selectedSeason ? season : -1
        });
        this.props.fetchSeasonRaces(season);
    };

    render() {
        const {items} = this.props;
        const {selectedSeason} = this.state;

        return (
            <SeasonsList items={items} selectedSeason={selectedSeason} onSeasonSelect={this.selectSeason}/>
        )
    };
}

SeasonsListContainer.propTypes = {
    items: PropTypes.arrayOf(
        seasonPropType
    )
};

SeasonsListContainer.defaultProps = {
    items: []
};

const mapDispatchToProps = (dispatch) => ({
    fetchSeasonRaces: (season) => dispatch(fetchSeasonRaces(season))
});

export default connect(
    null,
    mapDispatchToProps
)(SeasonsListContainer);
