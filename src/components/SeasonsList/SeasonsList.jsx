import React from 'react';
import PropTypes from 'prop-types';

import Season, {seasonPropType} from '../Season';

/**
 * Display the list of season winners.
 */
const SeasonsList = ({items, selectedSeason, onSeasonSelect}) => (
    <div className="winners-list">
        {
            items.map((item) =>
                <Season
                    key={item.season}
                    item={item}
                    selectItem={onSeasonSelect}
                    isSelected={item.season === selectedSeason}
                />
            )
        }
    </div>
);

SeasonsList.propTypes = {
    items: PropTypes.arrayOf(
        seasonPropType
    ).isRequired,
    selectedSeason: PropTypes.number,
    onSeasonSelect: PropTypes.func.isRequired
};

export default SeasonsList;
