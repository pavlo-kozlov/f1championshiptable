import React, {Component} from 'react';
import {connect} from 'react-redux';

import SeasonsList from './components/SeasonsList';
import {fetchSeasonWinners} from './actions';

const START_SEASON = 2005;
const END_SEASON = 2015;
let SEASONS = [];

/**
 * Initialize array of seasons (seasons range).
 */
(function initSeasons() {
    SEASONS = [];
    for (let i = START_SEASON; i <= END_SEASON; i++) {
        SEASONS.push(i);
    }
})();

/**
 * Application class that includes React components.
 */
class App extends Component {

    /**
     * Lifecycle method that called when component mounted in DOM.
     */
    componentDidMount() {
        this.props.fetchSeasonWinners(SEASONS);
    }

    /**
     * @returns {XML} The application component.
     */
    render() {
        const {seasonWinners} = this.props;
        return (
            <SeasonsList items={seasonWinners}/>
        );
    }
}

const mapStateToProps = (state) => ({
    seasonWinners: state.seasonWinners
});

const mapDispatchToProps = (dispatch) => ({
    fetchSeasonWinners: (seasons) => dispatch(fetchSeasonWinners(seasons))
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(App);
