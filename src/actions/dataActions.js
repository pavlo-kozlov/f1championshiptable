import {FETCH_SEASON_WINNERS_SUCCESS, FETCH_SEASON_SUCCESS} from '../constants/actionTypes';

/**
 * Action that fires when fetching season winner succeed.
 *
 * @param {Array} payload - The array of season winners.
 */
export const fetchSeasonWinnersSuccess = (payload) => ({
    type: FETCH_SEASON_WINNERS_SUCCESS,
    payload
});

/**
 * Action that fires when fetching season data succeed.
 *
 * @param {Number} season - The season number.
 * @param {Object} payload - The season details.
 */
export const fetchSeasonSuccess = (season, payload) => ({
    type: FETCH_SEASON_SUCCESS,
    season,
    payload
});
