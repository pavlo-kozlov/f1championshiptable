import {fetchSeasonWinnersSuccess, fetchSeasonSuccess} from './dataActions';
import {getSeasonWinner, getSeasonRaces} from '../business/services';
import {compareSeasons} from '../business/helpers';

/**
 * Action for fetching season winners.
 *
 * @param {Array} seasonsList - The list of seasons.
 */
export const fetchSeasonWinners = (seasonsList) => {
    return (dispatch) => {
        let seasonWinnerPromises = [];
        for (let i = 0; i < seasonsList.length; i++) {
            const seasonWinner = getSeasonWinner(seasonsList[i]);
            seasonWinnerPromises.push(seasonWinner);
        }
        Promise.all(seasonWinnerPromises)
            .then((results) => {
                const seasonWinners = results
                    .map((item) => ({
                        season: item.season,
                        winner: item,
                        raceWinners: []
                    }))
                    .sort(compareSeasons);
                dispatch(fetchSeasonWinnersSuccess(seasonWinners));
            })
    };
};

/**
 * Action for fetching season races.
 *
 * @param {Number} season - The season number.
 */
export const fetchSeasonRaces = (season) => {
    return (dispatch) => {
        getSeasonRaces(season)
            .then((result) => {
                dispatch(fetchSeasonSuccess(season, result));
            });
    };
};
