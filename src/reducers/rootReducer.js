import {combineReducers} from 'redux';

import {FETCH_SEASON_WINNERS_SUCCESS, FETCH_SEASON_SUCCESS} from '../constants/actionTypes';

/**
 * Season winners reducer.
 *
 * @param {Array} store
 * @param {Object} action
 * @returns {Array}
 */
const seasonWinners = (store = [], action) => {
    switch (action.type) {
        case FETCH_SEASON_WINNERS_SUCCESS:
            return action.payload;
        case FETCH_SEASON_SUCCESS:
            return store.map((seasonWinner) => {
                if (seasonWinner.season === action.season) {
                    seasonWinner.raceWinners = action.payload;
                }
                return seasonWinner;
            });
        default:
            return store;
    }
};

export default combineReducers({
    seasonWinners
});
