/**
 * Compile driver name.
 *
 * @param {String} givenName The first name.
 * @param {String} familyName The family name.
 * @returns {String}
 */
const compileName = (givenName, familyName) => (givenName + ' ' + familyName);

/**
 * Parse season winner from raw data.
 *
 * @param {Object} data The object to parse.
 * @returns {{season: Number, name: String, driverId: String}}
 */
export const parseSeasonWinner = (data) => {
    const standingsTable = data.MRData.StandingsTable;
    const driverStanding = standingsTable.StandingsLists[0].DriverStandings[0];
    const {familyName, givenName, driverId} = driverStanding.Driver;
    return {
        season: +standingsTable.season,
        name: compileName(givenName, familyName),
        driverId
    };
};

/**
 * Parse the list of races of the season.
 *
 * @param {Object} data The object to parse.
 * @returns {Array} The array of race winners by the year.
 */
export const parseSeasonRaces = (data) => {
    const races = data.MRData.RaceTable.Races;
    const result = [];
    for (const race of races) {
        const {raceName} = race;
        const {familyName, givenName, driverId} = race.Results[0].Driver;
        result.push({
            raceName,
            driverId,
            name: compileName(givenName, familyName)
        });
    }
    return result;
};

/**
 * Compare two seasons items.
 *
 * @param {Object} a The first item to compare.
 * @param {Number} a.season The season number.
 * @param {Object} b The second item to compare.
 * @param {Number} b.season The season number.
 * @returns {Number} The comparison result.
 */
export const compareSeasons = (a, b) => {
    if (a.season < b.season) {
        return -1;
    }

    // No need to check for equality
    return 1;

};
