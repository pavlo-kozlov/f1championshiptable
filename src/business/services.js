import 'whatwg-fetch';

import {parseSeasonRaces, parseSeasonWinner} from './helpers';

const SEASON_KEY = ':season';

const BASE_URL = 'http://ergast.com/api/f1/';

const SEASON_RACE_URL = `${BASE_URL}${SEASON_KEY}/results/1.json`;
const SEASON_WINNER_URL = `${BASE_URL}${SEASON_KEY}/driverStandings/1.json`;

/**
 * Load data by url and convert response to JSON format.
 *
 * @param {String} url The url to api method.
 * @returns {*|Promise.<JSON>}
 */
const toJsonDecorator = (url) =>
    fetch(url)
        .then((response) => response.json());

/**
 * Load the list of races by season.
 *
 * @param {Number} season The season year.
 * @returns {Promise.<JSON>}
 */
export const getSeasonRaces = (season)  =>
    toJsonDecorator(SEASON_RACE_URL.replace(SEASON_KEY, season))
        .then((data) => parseSeasonRaces(data));

/**
 * Load winner of the season.
 *
 * @param {Number} season The season year.
 * @returns {Promise.<JSON>}
 */
export const getSeasonWinner = (season) =>
    toJsonDecorator(SEASON_WINNER_URL.replace(SEASON_KEY, season))
        .then((data) => parseSeasonWinner(data));
